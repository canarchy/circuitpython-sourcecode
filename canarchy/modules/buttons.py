# Late 2022
# Author overflo
# Part of CANarchy
# Filename: buttons.py
# Purpose: Touchinputs on the back
# License Details found @ /LICENSE file in this repository


import time
import board
from digitalio import DigitalInOut, Direction, Pull
import touchio


try:
    from app import button_callback_onTouch
except:
    print("button_callback_onTouch() not defined in app.py")


class Buttons:
    buttons = []
    labels = []
    canarchy = None

    def __init__(self, canarchy):
        self.canarchy = canarchy
        self.canarchy.info(self.canarchy, "[x] Buttons")

    def addButton(self, pin_no, label):
        tbut = touchio.TouchIn(pin_no)
        tbut.threshold = 20000

        self.buttons.append(tbut)
        self.labels.append(label)

    def work(self):
        for index, btn in enumerate(self.buttons):

            if btn.value:
                try:
                    button_callback_onTouch(self.labels[index])
                except Exception as e:
                    self.canarchy.error(self.canarchy, "button_callback_onTouch() FAIL")
