####
# Early 2024
# Author deadcow - small adaptions by overflo
# Addition to CANarchy
# Filename: wlan.py
# Purpose: implements wifi-ap, server and serves webpage for controlling "touch"-buttons
# License Details found @ /LICENSE file in this repository

import os
import wifi
import socketpool
from adafruit_httpserver import Server, Request, Response
import app


class WiFiControl:
    def __init__(self, canarchy, ssid, password):
        self.ssid = ssid
        self.password = password
        self.server = None
        self.canarchy = canarchy
        self.active = True
        # TODO implement onConnect here and in app.py as callback
        self.client_connected = False

    def setup_wifi_ap(self):
        wifi.radio.start_ap(self.ssid, password=self.password)

    def start_http_server(self):
        pool = socketpool.SocketPool(wifi.radio)
        self.server = Server(pool, "/static", debug=True)

        @self.server.route("/")
        def base(request: Request):
            return Response(request, f"{webpage_light()}", content_type="text/html")

        @self.server.route("/on")
        def base(request: Request):
            app.on()

        @self.server.route("/off")
        def base(request: Request):
            app.off()

        @self.server.route("/eject")
        def base(request: Request):
            app.eject()

        self.server.start(str(wifi.radio.ipv4_address_ap))

    def work(self):
        self.server.poll()


def webpage_light():
    html = f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>CANarchy Control</title>
        <style>
            body {{
                background-color: #f0f0f0;
                color: #333;
                font-family: 'Courier New', Courier, monospace;
                text-align: center;
                margin-top: 50px;
            }}
            button {{
                background-color: #e0e0e0;
                color: #333;
                border: 1px solid #555;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                transition-duration: 0.4s;
                cursor: pointer;
            }}
            button:hover {{
                background-color: #c0c0c0;
                color: black;
            }}
        </style>
        <script>
            function sendCommand(command) {{
                fetch('/' + command)
                    .then(response => response.text())
                    .then(data => console.log(data));
            }}
        </script>
    </head>
    <body>
        <h2>CANarchy Control Panel</h2>
        <button onclick="sendCommand('on')">On</button>
        <button onclick="sendCommand('off')">Off</button>
        <button onclick="sendCommand('eject')">Eject</button>
    </body>
    </html>
    """
    return html
