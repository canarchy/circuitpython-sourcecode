# Example app.py
# Needs canarchy.io Firmware 2.5


from canarchy import canarchy as c

canarchy = c.CANarchy()


def app_init():
    # start an accesspoint with essid and password.
    # webinterfaces are available on http://192.168.4.1
    essid = "CANarchy"
    wifi_password = "12345678"

    canarchy.info("Running app_init()")
    canarchy.setup_wifi(essid, wifi_password)
    canarchy.set_can_bitrate(250000)


def on():
    canarchy.info(f"app.on() Turn on")
    canarchy.toggle_bus_en()
    roll_on()


def off():
    global rollin
    rollin = False
    canarchy.info(f"app.off() Shutdown")
    canarchy.send("2342#1337")
    canarchy.toggle_bus_en()


def eject():
    canarchy.info(f"app.eject()")
    canarchy.toggle_bus_en()
    for i in range(3):
        canarchy.send("4223#ABCD")


rollin = False


def roll_on():
    global rollin
    rollin = True

    canarchy.send("0815#ACAB")


# Button handlers
def button_callback_onTouch(label):

    if label == "button1":
        canarchy.info("onTouch(button1)")
        off()

    if label == "button2":
        canarchy.info("onTouch(button2)")
        eject()

    if label == "button3":
        canarchy.info("onTouch(button3)")
        on()


# called as often as possible
last_callback = 0


import time


def periodic_callback():
    global last_callback
    now = time.monotonic()
    if last_callback == 0:
        last_callback = now

    # do something once per second
    if now - last_callback > 1:
        last_callback = now
        if rollin:
            roll_on()


def hall_callback_onChange(magnet_detected):
    if not magnet_detected:
        canarchy.info(f"Hall: {magnet_detected}")
        if not rollin:
            on()
        else:
            off()


# called on CAN receive or send, implement logging here or something else.
def can_callback_onReceive(message):
    return
    # print("can_callback_onReceive")
    # data = message.data
    # id_str = str(hex(message.id))[2:]


def can_callback_onSend(message):
    return
    # print("can_callback_onSend")
    # data = message.data
    # id_str = str(hex(message.id))[2:]
    # print("CAN > %s#%s"%(f'{id_str:0>8}',hexlify(message.data).decode().upper()))
