# not much to see here.
# only basic setup before diving into canarchy


import sys

# add location of modules and lib to sys.path at the very end
sys.path.append("/canarchy/lib")
sys.path.append("/canarchy/modules")


try:
    import busio, sdcardio, storage, board

    s = sdcardio.SDCard(busio.SPI(board.SCK, board.MOSI, board.MISO), board.IO5)
    vfs = storage.VfsFat(s)
    storage.mount(vfs, "/sd")

    # SD first!
    sys.path.insert(
        0, "/sd/canarchy/modules"
    )  # load 3rd party modules from SD per default
    sys.path.insert(0, "/sd/canarchy/lib")  # load libs from SD per default
    sys.path.insert(0, "/sd")  # to override app.py from /sd root

except Exception as e:
    print("SD Card setup failed :( ", e)


# check out /canarchy/canarchy.py for all the fun.
from canarchy import canarchy as c

# create the canarchy SINGLETON
canarchy = c.CANarchy()

# never return
canarchy.start()
